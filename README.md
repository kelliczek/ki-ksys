# App Bürgerfestus

## User story

Budeme simulovat vchod, který je plně obsluhován veřejnou dopravou: autobus pravidelně vysadí několik návštěvníků, kteří si pak budou muset nechat naskenovat vstupenky před vstupem na akci burgerů. Někteří návštěvníci budou mít vstupenky, které si předem zakoupili, zatímco jiní budou muset nejprve přistoupit k prodejním stánkům, aby si vstupenky koupili. 

Ke složitosti přispívá i to, že když se návštěvníci přiblíží ke stánku prodejců, budou tak činit ve skupinách (simulují rodinný/skupinový nákup vstupenek); každá osoba si však bude muset nechat naskenovat vstupenky samostatně.


<br>


## Předpoklady

Abychom to mohli simulovat, budeme se muset rozhodnout, jak reprezentovat tyto různé události pomocí rozdělení pravděpodobnosti. Předpoklady provedené v implementaci zahrnují:

* Autobus přijíždí v průměru 1 každé 3 minuty - exponenciální rozdělení s λ 1/3
* Každý autobus bude obsahovat 100 +/- 30 návštěvníků určených pomocí normálního rozložení (μ = 100, σ = 30)
* Návštěvníci vytvoří skupiny po 2,25 +/– 0,5 osob za použití normálního rozdělení (μ = 2,25, σ = 0,5) - Toto zaokrouhlíme na nejbližší celé číslo
* Budeme předpokládat, že pevně stanovený poměr 40 % návštěvníků si bude muset zakoupit vstupenky na stáncích prodejce, dalších 40 % dorazí se vstupenkou již zakoupenou online a 20 % dorazí s pověřením personálu.
* Návštěvníkům bude trvat v průměru 1 minutu, než vystoupí z autobusu a dojdou ke stánku prodejce (normální, μ = 1, σ = 0,25), a další půl minuty chůze od prodejců ke skenerům (normální, μ = 0,5, σ = 0,1). U těch, kteří přeskakují prodejce (předem zakoupené vstupenky), budeme předpokládat průměrnou procházku 1,5 minuty (normální, μ = 1,5, σ = 0,35)
* Návštěvníci si při příchodu vyberou nejkratší linku, kde každá linka má jednoho prodejce nebo skener
* Prodej vyžaduje 1 +/- 0,2 minuty na dokončení (normální, μ = 1, σ = 0,2)
* Dokončení skenování vyžaduje 0,05 +/- 0,01 minuty (normální, μ = 0,05, σ = 0,01)


<br>


## Grafy

### Vlevo

Graf na levé straně představuje počet návštěvníků přicházejících za minutu.


### Vpravo

Grafy na pravé straně představují průměrnou dobu, po kterou návštěvníci opouštějící frontu v tu chvíli potřebovali čekat, než budou obslouženi.


<br>

## Konfigurace

Konfigurace je dostupná v `config.py`, kde je možné nastavovat proměnné, které ovlivňují chod programu.

*Varování: Pokud se zadají nesmyslné hodnoty, aplikace spadne*


<br>
<hr>
<br>


# Setup

## Run

1. Switch to virtual env
	```shell
	source venv/bin/activate 
	```

1. Run app
	```shell
	python3 main.py
	```

1. Watch until you get bored or app stops/crashes 

1. Deactivate virtual env
	```shell
	deactivate
	```

<br>

## Run locally (dev)

```shell
pip3 install numpy
```

```shell
pip3 install simpy
```

```shell
pip3 install tk
```

```shell
pip3 install matplotlib
```

```shell
python3 main.py
```


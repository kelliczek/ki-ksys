#!/usr/bin/env python3

# UTIL FILE

from collections import defaultdict
import numpy as np
from config import *

arrivals = defaultdict(lambda: 0)
seller_waits = defaultdict(lambda: [])
scan_waits = defaultdict(lambda: [])
event_log = []


def log(message):
	if LOG_DATA: print(message)

def register_arrivals(time, num):
    arrivals[int(time)] += num

def register_seller_wait(time, wait):
    seller_waits[int(time)].append(wait)

def register_scan_wait(time, wait):
    scan_waits[int(time)].append(wait)

def avg_wait(raw_waits):
    waits = [ w for i in raw_waits.values() for w in i ]
    return round(np.mean(waits), 1) if len(waits) > 0 else 0

def register_bus_arrival(time, bus_id, people_created):
    register_arrivals(time, len(people_created))
    log(f"Bus #{bus_id} arrived at {time} with {len(people_created)} feasters")
    event_log.append({
        "event": "BUS_ARRIVAL",
        "time": round(time, 2),
        "busId": bus_id,
        "peopleCreated": people_created
    })

def register_group_moving_from_bus_to_seller(people, walk_begin, walk_end, seller_line, queue_begin, queue_end, sale_begin, sale_end):
    wait = queue_end - queue_begin
    service_time = sale_end - sale_begin
    register_seller_wait(queue_end, wait)
    log(f"Purchasing group of {len(people)} waited {wait} minutes in Line {seller_line}, needed {service_time} minutes to complete")
    event_log.append({
        "event": "WALK_TO_SELLER",
        "people": people,
        "sellerLine": seller_line,
        "time": round(walk_begin, 2),
        "duration": round(walk_end - walk_begin, 2)
    })
    event_log.append({
        "event": "WAIT_IN_SELLER_LINE",
        "people": people,
        "sellerLine": seller_line,
        "time": round(queue_begin, 2),
        "duration": round(queue_end - queue_begin, 2)
    })
    event_log.append({
        "event": "BUY_TICKETS",
        "people": people,
        "sellerLine": seller_line,
        "time": round(sale_begin, 2),
        "duration": round(sale_end - sale_begin, 2)
    })

def register_visitor_moving_to_scanner(person, walk_begin, walk_end, scanner_line, queue_begin, queue_end, scan_begin, scan_end):
    wait = queue_end - queue_begin
    service_time = scan_end - scan_begin
    register_scan_wait(queue_end, wait)
    log(f"Scanning feaster waited {wait} minutes in Line {scanner_line}, needed {service_time} minutes to complete")
    event_log.append({
        "event": "WALK_TO_SCANNER",
        "person": person,
        "scannerLine": scanner_line,
        "time": round(walk_begin, 2),
        "duration": round(walk_end - walk_begin, 2)
    })
    event_log.append({
        "event": "WAIT_IN_SCANNER_LINE",
        "person": person,
        "scannerLine": scanner_line,
        "time": round(queue_begin, 2),
        "duration": round(queue_end - queue_begin, 2)
    })
    event_log.append({
        "event": "SCAN_TICKETS",
        "person": person,
        "scannerLine": scanner_line,
        "time": round(scan_begin, 2),
        "duration": round(scan_end - scan_begin, 2)
    })


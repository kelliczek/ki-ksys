#!/usr/bin/env python3

import random
import simpy
import os
import json
from datetime import datetime
from config import *
from utils import *
from ui import *



#  SIMULATION

def pick_shortest(lines):
    shuffled = list(zip(range(len(lines)), lines)) # tuples of (i, line)
    random.shuffle(shuffled)
    shortest = shuffled[0][0]
    for i, line in shuffled:
        if len(line.queue) < len(lines[shortest].queue):
            shortest = i
            break
    return (lines[shortest], shortest + 1)

def create_clock(env):
   
    while True:
        yield env.timeout(0.1)
        clock.tick(env.now)

def bus_arrival(env, seller_lines, scanner_lines):
    # Note that these unique IDs for busses and people are not required, but are included for eventual visualizations 
    next_bus_id = 0
    next_person_id = 0
    while True:
        # next_bus = random.expovariate(1 / BUS_ARRIVAL_MEAN)        
        # on_board = int(random.gauss(BUS_OCCUPANCY_MEAN, BUS_OCCUPANCY_STD))        
        next_bus = ARRIVALS.pop()
        on_board = ON_BOARD.pop()
        
        # Wait for the bus 
        bus_log.next_bus(next_bus)
        yield env.timeout(next_bus)
        bus_log.bus_arrived(on_board)
        
        # register_bus_arrival() below is for reporting purposes only 
        people_ids = list(range(next_person_id, next_person_id + on_board))
        register_bus_arrival(env.now, next_bus_id, people_ids)
        next_person_id += on_board
        next_bus_id += 1

        while len(people_ids) > 0:
            remaining = len(people_ids)
            group_size = min(round(random.gauss(PURCHASE_GROUP_SIZE_MEAN, PURCHASE_GROUP_SIZE_STD)), remaining)
            people_processed = people_ids[-group_size:] # Grab the last `group_size` elements
            people_ids = people_ids[:-group_size] # Reset people_ids to only those remaining

            # Randomly determine if this group is going to the sellers or straight to the scanners
            if random.random() > PURCHASE_RATIO_MEAN:
                env.process(scanning_customer(env, people_processed, scanner_lines, TIME_TO_WALK_TO_SELLERS_MEAN + TIME_TO_WALK_TO_SCANNERS_MEAN, TIME_TO_WALK_TO_SELLERS_STD + TIME_TO_WALK_TO_SCANNERS_STD))
            else:
                env.process(purchasing_customer(env, people_processed, seller_lines, scanner_lines))

def purchasing_customer(env, people_processed, seller_lines, scanner_lines):
    walk_begin = env.now
    yield env.timeout(random.gauss(TIME_TO_WALK_TO_SELLERS_MEAN, TIME_TO_WALK_TO_SELLERS_STD))
    walk_end = env.now

    queue_begin = env.now
    seller_line = pick_shortest(seller_lines)
    with seller_line[0].request() as req:
        # Wait in line
        sellers.add_to_line(seller_line[1])
        yield req
        sellers.remove_from_line(seller_line[1])
        queue_end = env.now

        # Buy tickets
        sale_begin = env.now
        yield env.timeout(random.gauss(SELLER_MEAN, SELLER_STD))
        sale_end = env.now

        register_group_moving_from_bus_to_seller(people_processed, walk_begin, walk_end, seller_line[1], queue_begin, queue_end, sale_begin, sale_end)
        
        env.process(scanning_customer(env, people_processed, scanner_lines, TIME_TO_WALK_TO_SCANNERS_MEAN, TIME_TO_WALK_TO_SCANNERS_STD))

def scanning_customer(env, people_processed, scanner_lines, walk_duration, walk_std):
    # Walk to the seller 
    walk_begin = env.now
    yield env.timeout(random.gauss(walk_duration, walk_std))
    walk_end = env.now

    # We assume that the visitor will always pick the shortest line
    queue_begin = env.now    
    scanner_line = pick_shortest(scanner_lines)
    with scanner_line[0].request() as req:
        # Wait in line
        for _ in people_processed: scanners.add_to_line(scanner_line[1])
        yield req
        for _ in people_processed: scanners.remove_from_line(scanner_line[1])
        queue_end = env.now
        
        # Scan each person's tickets 
        for person in people_processed:
            scan_begin = env.now
            yield env.timeout(random.gauss(SCANNER_MEAN, SCANNER_STD)) # Scan tickets
            scan_end = env.now
            register_visitor_moving_to_scanner(person, walk_begin, walk_end, scanner_line[1], queue_begin, queue_end, scan_begin, scan_end)


#env = simpy.rt.RealtimeEnvironment(factor = 0.01, strict = False) # Uncomment to run in realtime (slow)
env = simpy.Environment()

seller_lines = [ simpy.Resource(env, capacity = SELLERS_PER_LINE) for _ in range(SELLER_LINES) ]
scanner_lines = [ simpy.Resource(env, capacity = SCANNERS_PER_LINE) for _ in range(SCANNER_LINES) ]

# Simulating bus arrival time and people count
env.process(bus_arrival(env, seller_lines, scanner_lines))

env.process(create_clock(env))

env.run(until = RUN_TIME) # run until X min

main.mainloop()

if LOG_DATA:
	if not os.path.exists('./output'):
		os.mkdir('./output')

	with open(f'output/data-{datetime.now()}.json', 'w') as outfile:
			json.dump({
					"sellerLines": SELLER_LINES,
					"scannerLines": SCANNER_LINES,
					"events": event_log
			}, outfile)